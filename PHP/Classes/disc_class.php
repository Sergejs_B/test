<?php
	class Disc extends Product {
		function __construct(string $sku,string $name,string $price, string $attribute){
			parent:: __construct($sku, $name, $price, $attribute);
			$this->setAttribute($attribute);
		}
		
		function setAttribute($disc) {
			$this->attribute = 'Size: ' .$disc." MB"  ;
		}
	}

