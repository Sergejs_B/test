<?php
	class Product {

		private $sku;
		private $name;
		private $price;
		public $attribute;

		public function __construct(string $sku,string $name,string $price, string $attribute) {
			$this->sku = $sku;
			$this->name = $name;
			$this->price = $price;
			$this->attribute = $attribute;
		}

		public function getSku() {
			return $this->sku;
		}

		public function getName() {
			return $this->name;
		}

		public function getPrice() {
			return $this->price;
		}

		public function getAttribute() {
			return $this->attribute;
		}
		
	}