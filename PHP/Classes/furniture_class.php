<?php 
    class Furniture extends Product{
		function __construct(string $sku, string $name, string $price, string $attribute){
			parent::__construct($sku, $name, $price, $attribute);
			$this->setAttribute($attribute);
		}

		function setAttribute($dimensions) {
			$this->attribute = 'Dimensions: '. $dimensions .' cm';
		}
	}


    
 