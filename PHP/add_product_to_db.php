<?php
	require_once("Classes/product_class.php");
	require_once("Classes/disc_class.php");
	require_once("Classes/furniture_class.php");
	require_once("Classes/book_class.php");
	require_once("Classes/db_manager.php");

	$db = new DbManager();
	
	// Validation
	$sku = $_POST['SKU'];
	$validation = $db->findSku($sku);

	if ($validation->num_rows) {
		echo "SKU already exists";
		?><button onclick="window.location.href='/product.php'" class="navig_btn">Add new product</button><?
	} else {
		switch ($_POST["swithcer"]) {
			case "disc":
				$product = new Disc ($_POST['SKU'], $_POST['name'], $_POST['price'], $_POST['disc']);
				$db->add($product);
				break;

			case "book";
				$product = new Book ($_POST['SKU'], $_POST['name'], $_POST['price'], $_POST['book']);
				$db->add($product);
				break;
			
			case "furniture":
				$product = new Furniture ($_POST['SKU'], $_POST['name'], $_POST['price'], $_POST['height'].' x '. $_POST['width'].' x '.$_POST['length']);
				$db->add($product);
				break;

			default:
	        	echo 'Something went wrong';
	    		break;
		}
	}

	