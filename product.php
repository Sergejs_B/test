<!DOCTYPE html>
<html>
	<head>
		<? require_once 'PHP/Classes/database.php';?>
		<title>Product Add</title>
		<script src="JS/validation.js"></script>
		<link rel="stylesheet" type="text/css" href="CSS/style.css">
	</head>
	<body>
		<button onclick="window.location.href='/index.php'" class="navig_btn">To product list</button>   
		<h1>Product Add</h1>
		<hr>
			<div class ="dynamicbox">
				<form method="POST" action="PHP/add_product_to_db.php" id="dynform">
					<label for="sku">SKU</label>
					<input type="text" name="SKU" required class="dyninput">

					<label for="name">Name</label>
					<input type="text" name="name" required class="dyninput">	

					<label for="price">Price</label>
					<input type="text" name="price" required onkeypress="javascript:return isNumber(event)" class="dyninput">	

					<label for="swithcer">Type Switcher</label>
					<select id="switcher" name="swithcer">
						<option value="disc" >DVD disc</option>
						<option value="book" name="book">Book</option>
						<option value="furniture">Furniture</option>
					</select> 
				</form>
			</div>
	<script src="JS/atribute.js"></script>
	</body>
</html>
