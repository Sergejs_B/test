<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <? require_once 'PHP/Classes/database.php';
        require_once 'PHP/get_product_from_db.php';?>
        <link rel="stylesheet" type="text/css" href="CSS/style.css">
        <title>Product List</title>
    </head>
    <body>
        <button onclick="window.location.href='/product.php'" class="navig_btn">Add new product</button>
        <h1>Product List</h1> 
        <hr>
        <div class="product_table">
            <?php while ($row =$result->fetch_assoc()) { 
                display_products(); 
                $products = new Product($row['sku'],$row['name'],$row['price'],$row['attribute']); 
                ?>
                <div class="container">
          		    <p>SKU: <? echo ($products->getSku())?></p>
          		    <p>Name: <? echo ($products->getName())?></p>
          		    <p>Price: <? echo ($products->getPrice())?> €</p>
          		    <p><? echo ($products->getAttribute())?></p>
                </div>
      	 <? } ?>
        </div>
    </body>
</html>